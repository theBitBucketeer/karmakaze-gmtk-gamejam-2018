﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager {

	#region Events/Delegates
	public delegate void StartGameAction();
	public static event StartGameAction startGameEvent;

	public delegate void UpdateScoreAction (int points);
	public static event UpdateScoreAction updateScoreEvent;

	public delegate void UpdateHealthAction (int health);
	public static event UpdateHealthAction updateHealthEvent;

	public delegate void UpdateLivesAction (int lives);
	public static event UpdateLivesAction updateLivesEvent;

	public delegate void GameOverAction ();
	public static event GameOverAction gameOverEvent;

	public delegate void HitBonusAction (int hitBonus);
	public static event HitBonusAction hitBonusEvent;

	public delegate void PlayerHealthGainAction (int health);
	public static event PlayerHealthGainAction playerHealthGainEvent;
	#endregion

	#region Event-Calling Methods
	public static void StartGame () {
		if (startGameEvent != null) {
			startGameEvent ();
		}	
	}

	public static void UpdateScore (int points) {
		if(updateScoreEvent != null) {
			updateScoreEvent (points);
		}
	}

	public static void UpdateHealth (int health) {
		if(updateHealthEvent != null) {
			updateHealthEvent (health);
		}
	}

	public static void UpdateLives (int lives) {
		if(updateLivesEvent != null) {
			updateLivesEvent (lives);
		}
	}

	public static void GameOver () {
		if(gameOverEvent != null) {
			gameOverEvent ();
		}
	}

	public static void HitBonus (int hitBonus) {
		if(hitBonusEvent != null) {
			hitBonusEvent (hitBonus);
		}
	}

	public static void PlayerHealthGain (int health) {
		if(playerHealthGainEvent != null) {
			playerHealthGainEvent (health);
		}
	}
	#endregion
}

