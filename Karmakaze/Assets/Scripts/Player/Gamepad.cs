﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gamepad {

	/* The left joystick's horizontal axis */
	private float leftHorizontal;

	/* The left joystick's vertical axis */
	private float leftVertical;

	public Gamepad() {
		this.leftVertical = 0.0f;
		this.leftHorizontal = 0.0f;
	}

	/* Getters/Setters */
	public float LeftHorizontal {
		get {
			return this.leftHorizontal;
		}
		set {
			leftHorizontal = value;
		}
	}

	public float LeftVertical {
		get {
			return this.leftVertical;
		}
		set {
			leftVertical = value;
		}
	}
}
