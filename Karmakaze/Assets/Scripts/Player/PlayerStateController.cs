﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStateController : MonoBehaviour, IDamageable {

	/* the player's health */
	private int health = 100;

	/* the player's lives */
	private int lives = 3;

	/* the speed of the player's movement */
	public int speed;

	/* the amount of additional damage the player is currently doing */
	public int fireDamage = 0;

	/* the acceleration for the player movement after bouncing */
	public float accerelation;

	/* the gamepad for the player */
	private Gamepad gamepad;

	/* The rigidbody 2d for this player */
	private Rigidbody2D rBody;

	/* the collider for this player */
	private Collider2D coll;

	/* did the player just hit an enemy */
	private bool hitEnemy = false;

	/* has the game started yet? */
	private bool isGameStarted = false;

	/* the transform for the player */
	private Transform tform;

	/* is the player respawning? */
	private bool isRespawning;

	/* the engine sound */
	public AudioSource engineSound;

	/* the death sound */
	public AudioSource deathSound;

	/* the hit sound */
	public AudioSource hitSound;

	public GameObject hit;

	// Use this for initialization
	void Start () {
		this.health = 100;
		this.fireDamage = 0;
		this.gamepad = new Gamepad ();
		this.rBody = GetComponent<Rigidbody2D> ();
		this.tform = GetComponent<Transform> ();
		this.coll = GetComponent<Collider2D> ();
	}

	void OnEnable () {
		EventManager.startGameEvent += StartGame;
		EventManager.playerHealthGainEvent += GainHealth;
	}

	void OnDisable () {
		EventManager.startGameEvent -= StartGame;
		EventManager.playerHealthGainEvent -= GainHealth;
	}
	
	// Update is called once per frame
	void Update () {
		if(this.isGameStarted && !this.isRespawning) {
			this.UpdateInput ();
		}
	}

	private void StartGame () {
		this.engineSound.Play ();
		this.isGameStarted = true;
	}

	private void UpdateInput() {
		//grab the joystick data at the beginning of every frame
		this.gamepad.LeftHorizontal = Input.GetAxis ("Horizontal");
		this.gamepad.LeftVertical = Input.GetAxis ("Vertical");

		if(!this.hitEnemy) {
			this.rBody.velocity = new Vector2 (this.gamepad.LeftHorizontal, this.gamepad.LeftVertical) * this.speed;
		} else {
			Vector2 speedAddition = new Vector2 (this.gamepad.LeftHorizontal, this.gamepad.LeftVertical) * this.accerelation;
			this.rBody.AddForce(speedAddition);
			float xVelocity = this.rBody.velocity.x;
			float yVelocity = this.rBody.velocity.y;

			if(Mathf.Abs(xVelocity) > this.speed) {
				xVelocity = Mathf.Sign (xVelocity) * this.speed;
			}

			if(Mathf.Abs(yVelocity) > this.speed) {
				yVelocity = Mathf.Sign (yVelocity) * this.speed;
			}
			this.rBody.velocity = new Vector2 (xVelocity, yVelocity);
		}
	}

	/* IMPLEMENTATION: called when a damageable object should take damage */
	public void TakeDamage (int damageAmount) {
		if(!this.isRespawning && this.isGameStarted) {
			this.health -= damageAmount;
			if (!this.hitSound.isPlaying){
				this.hitSound.Play ();
			}
			if(this.health <= 0) {
				this.health = 0;
				this.Kill ();
			} else if(this.health < 50) {
				this.fireDamage = 50 - this.health;
				EventManager.HitBonus (this.fireDamage);
			}
			EventManager.UpdateHealth(this.health);
		}
	}	

	/* IMPLEMENTATION: called when a damageable object should gain health back */
	public void GainHealth (int healthAmount) {
		this.health += healthAmount;
		if(this.health > 100) {
			this.health = 100;
		} else if(this.health >= 50) {
			this.fireDamage = 0;
			EventManager.HitBonus (0);
		}
		EventManager.UpdateHealth(this.health);
	}

	/* IMPLEMENTATION: Called when a damageable object should be destroyed/killed */
	public void Kill () {
		this.lives--;
		this.deathSound.Play ();
		this.engineSound.Stop ();
		EventManager.HitBonus (0);
		if(this.lives <= -1) {
			this.isGameStarted = false;
			Respawn ();
			this.lives = 3;
			EventManager.GameOver ();
		} else {
			Respawn ();
			EventManager.UpdateLives (this.lives);
		}
	}

	public void Respawn () {
		this.coll.enabled = false;
		this.health = 100;
		this.fireDamage = 0;
		this.rBody.velocity = Vector2.zero;
		this.tform.position = new Vector3 (0f, -3.5f, 0f);
		this.isRespawning = true;
		StartCoroutine (WaitRespawning ());
	}

	IEnumerator WaitRespawning () {
		yield return new WaitForSeconds (1.5f);
		this.isRespawning = false;
		this.coll.enabled = true;
		this.engineSound.Play ();
	}

	void OnCollisionEnter2D (Collision2D coll) {
		if(coll.gameObject.CompareTag("Enemy")){
			Enemy enemy = coll.gameObject.GetComponent<Enemy> ();
			int damage = 5 + this.fireDamage;
			Vector3 pos = new Vector3 (coll.contacts [0].point.x, coll.contacts [0].point.y, 0f);
			Instantiate (hit, pos, this.transform.rotation);
			enemy.TakeDamage (damage);
			StartCoroutine (HitEnemy ());
		}
	}

	IEnumerator HitEnemy () {
		this.hitEnemy = true;
		yield return new WaitForSeconds (0.2f);
		this.hitEnemy = false;
	}


}
