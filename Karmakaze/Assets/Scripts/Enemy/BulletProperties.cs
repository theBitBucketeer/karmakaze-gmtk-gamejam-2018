﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class BulletProperties : ScriptableObject {

	/* the amount of time between shots being fired */
	public float speed;

	/* the list of directions the bullets will be shot in over time */
	public int damageAmount;

}