﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class EnemySpawn {

	/* The index of the spawn point to spawn at */
	public int spawnIndex;

	/* the enemy to spawn */
	public GameObject enemy;

}