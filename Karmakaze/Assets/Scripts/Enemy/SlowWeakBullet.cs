﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowWeakBullet : MonoBehaviour, Bullet {

	/* the properties for this bullet */
	public BulletProperties bulletProperties;

	/* the direction this bullet is moving in */
	public Vector2 direction;

	/* the rigidbody2d for this bullet */
	private Rigidbody2D rBody;

	// Use this for initialization
	void Start () {
		rBody = GetComponent<Rigidbody2D> ();
		Destroy (gameObject, 10);
	}
	
	// Update is called once per frame
	void Update () {
		Move ();
	}

	public void Move() {
		this.rBody.velocity = this.direction * this.bulletProperties.speed;
	}

	public void SetDirection (Vector2 direction) {
		this.direction = direction;
	}

	void OnTriggerEnter2D (Collider2D coll) {
		if(coll.CompareTag("Player")) {
			PlayerStateController player = coll.GetComponent<PlayerStateController> ();
			player.TakeDamage (bulletProperties.damageAmount);
			Destroy (gameObject, 0.05f);
		}
	}
}
