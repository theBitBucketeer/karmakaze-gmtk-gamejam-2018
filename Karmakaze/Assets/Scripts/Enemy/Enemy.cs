﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour, IDamageable {

	/* the bullet pattern for this enemy */
	public BulletPattern bulletPattern;

	/* the enemy properties */
	public EnemyProperties enemyProperties;

	/* the enemy movement pattern */
	public MovementPattern movementPattern;

	/* the health for this enemy */
	private int health = 100;

	/* the transform of this enemy */
	private Transform tform;

	/* the current index in the directions of the bulletPattern */
	private int directionsIndex = 0;

	/* the current index in the directions of the movement pattern */
	private int movementIndex = 0;

	/* the collider for this enemy */
	private Collider2D coll;

	/* used for calculating the next position to move to */
	private Vector2 previousPos;

	/* did we activate the wait time for movement */
	private bool waiting = false;

	/* sound for killing vehicle */
	public AudioSource deathSound;

	/* sound for hitting vehicle */
	public AudioSource hitSound;



	// Use this for initialization
	void Start () {
		this.directionsIndex = 0;
		this.movementIndex = 0;
		this.health = enemyProperties.health;
		this.coll = GetComponent<Collider2D> ();
		this.tform = GetComponent<Transform> ();
		this.previousPos = this.tform.position;
		StartCoroutine (FireBulletPattern ());
	}

	void Update () {
		Move ();
	}

	void OnEnable () {
		EventManager.gameOverEvent += GameOver;
	}

	void OnDisable () {
		EventManager.gameOverEvent -= GameOver;
	}

	/* IMPLEMENTATION: called when a damageable object should take damage */
	public void TakeDamage (int damageAmount) {
		this.health -= damageAmount;
		if(this.health <= 0) {
			this.health = 0;
			this.Kill ();
			this.deathSound.Play ();
		} else {
			this.hitSound.Play ();
		}
	}	


	//	


	/* IMPLEMENTATION: called when a damageable object should gain health back */
	public void GainHealth (int healthAmount) {
		//Enemies can't gain health for now, do nothing
	}

	/* IMPLEMENTATION: Called when a damageable object should be destroyed/killed */
	public void Kill () {
		this.coll.enabled = false;
		StopAllCoroutines ();
		this.directionsIndex = 0;
		EventManager.UpdateScore (this.enemyProperties.points);
		EventManager.PlayerHealthGain (this.enemyProperties.healthGained);
		Destroy (gameObject, 1f);
	}

	private void GameOver () {
		StopAllCoroutines ();
		Destroy (gameObject, 1);
	}

	IEnumerator FireBulletPattern () {
		GameObject bullet = Instantiate(bulletPattern.bulletType, this.tform.position, this.tform.rotation);
		Bullet bulletScript = bullet.GetComponent<Bullet> ();
		bulletScript.SetDirection (bulletPattern.bulletDirections [this.directionsIndex]);
		this.directionsIndex++;
		if(this.directionsIndex == bulletPattern.bulletDirections.Length) {
			this.directionsIndex = 0;
		}
		yield return new WaitForSeconds (bulletPattern.timeBetweenShots);
		StartCoroutine (FireBulletPattern ());
	}

	private void Move () {
		//calculate the next point to move to
		Vector2 distanceToMove = this.movementPattern.movementDirections[this.movementIndex];
		Vector2 currentPos = new Vector2 (this.tform.position.x, this.tform.position.y);
		Vector2 nextPosition = previousPos + distanceToMove;

		//if we are close enough, wait for next movement
		if(Vector2.Distance(currentPos, nextPosition) > 0.1f) {
			//move towards the next point
			Vector2 newPos = Vector2.MoveTowards(currentPos, nextPosition, this.enemyProperties.movementSpeed * Time.deltaTime);
			this.tform.position = new Vector3 (newPos.x, newPos.y, 0f);
		} else if(!waiting){
			this.waiting = true;
			StartCoroutine (WaitForNextMove ());
		}
	}

	IEnumerator WaitForNextMove () {
		//get time to wait from movement list
		yield return new WaitForSeconds (this.movementPattern.timeBetweenMovements);
		this.previousPos = this.tform.position;
		this.waiting = false;
		this.movementIndex++;
		if(this.movementIndex >= this.movementPattern.movementDirections.Length) {
			//kill this enemy, they should be offscreen
			Destroy (gameObject);
		}
	}
}
