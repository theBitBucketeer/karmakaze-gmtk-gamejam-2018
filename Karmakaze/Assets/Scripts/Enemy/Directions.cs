﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class Directions {

	/* the directions that the bullet will alternate between */
	public Vector2 directions;

	/* the amount of time before it switches directions */
	public float timeBetweenDirections;
}
