﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class MovementPattern : ScriptableObject {

	/* the amount of time between moving to the next spot */
	public float timeBetweenMovements;

	/* the list of directions the enemies will move to over time */
	public Vector2[] movementDirections;
}