﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class EnemyList : ScriptableObject {

	/* the list of enemy spawn configurations */
	public EnemySpawnConfig[] enemySpawnList;

	/* the time in between spawns */
	public float timeBetweenSpawns;
}
