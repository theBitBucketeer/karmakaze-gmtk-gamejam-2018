﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface Bullet {

	/* moves the bullet based on the bullet type */
	void Move ();

	/* sets the direction for the bullet to move in */
	void SetDirection (Vector2 direction);
}
