﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class BulletPattern : ScriptableObject {

	/* the amount of time between shots being fired */
	public float timeBetweenShots;

	/* the list of directions the bullets will be shot in over time */
	public Vector2[] bulletDirections;

	/* the type of bullet to spawned */
	public GameObject bulletType;

}