﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class EnemyProperties : ScriptableObject {

	/* the amount of points killing this enemy is worth */
	public int points;

	/* the amount of health this enemy has */
	public int health;

	/* the speed of the enemy's movement */
	public float movementSpeed;

	/* the amount of health the player receives for killing this enemy */
	public int healthGained;

}