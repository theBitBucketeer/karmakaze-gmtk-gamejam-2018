﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {

	/* The array of spawn points */
	public Transform[] spawnPoints;

	/* the current index into the enemy list */
	private int enemyListIndex;

	/* the list of enemies to spawn */
	public EnemyList enemyList;

	// Use this for initialization
	void Start () {
		this.enemyListIndex = 0;
	}

	void OnEnable () {
		EventManager.startGameEvent += StartGame;
		EventManager.gameOverEvent += GameOver;
	}

	void OnDisable () {
		EventManager.startGameEvent -= StartGame;
		EventManager.gameOverEvent -= GameOver;
	}

	void StartGame () {
		StartCoroutine (SpawnEnemies ());
	}

	void GameOver () {
		StopAllCoroutines ();
		this.enemyListIndex = 0;
	}

	IEnumerator SpawnEnemies () {
		int numEnemiesToSpawn = enemyList.enemySpawnList [this.enemyListIndex].enemySpawns.Length;
		for(int i = 0; i < numEnemiesToSpawn; i++) {
			int spawnPosition = this.enemyList.enemySpawnList [this.enemyListIndex].enemySpawns [i].spawnIndex;
			Instantiate(this.enemyList.enemySpawnList [this.enemyListIndex].enemySpawns [i].enemy, this.spawnPoints[spawnPosition].position,
				this.spawnPoints[spawnPosition].rotation);
		}
		this.enemyListIndex++;
		if(this.enemyListIndex == this.enemyList.enemySpawnList.Length) {
			this.enemyListIndex = 0;
		}
		yield return new WaitForSeconds (enemyList.timeBetweenSpawns);
		StartCoroutine (SpawnEnemies ());
	}
}
