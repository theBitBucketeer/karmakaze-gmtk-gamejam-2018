﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDamageable {

	/* called when a damageable object should take damage */
	void TakeDamage (int damageAmount);

	/* called when a damageable object should gain health back */
	void GainHealth (int healthAmount);

	/* Called when a damageable object should be destroyed/killed */
	void Kill ();
}
