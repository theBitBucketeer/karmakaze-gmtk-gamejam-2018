﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class MusicPlayer : MonoBehaviour {

	public AudioSource musicSource;

	public AudioMixerSnapshot[] snapshots;

	void OnEnable () {
		EventManager.startGameEvent += StartGame;
		EventManager.gameOverEvent += GameOver;
	}

	void OnDisable () {
		EventManager.startGameEvent -= StartGame;
		EventManager.gameOverEvent -= GameOver;
	}

	void StartGame () {
		musicSource.Play ();
		snapshots [1].TransitionTo (0.4f);
	}

	void GameOver () {
		snapshots [0].TransitionTo (1.5f);
	}
}
