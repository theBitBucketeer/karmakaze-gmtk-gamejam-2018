﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollingBackground : MonoBehaviour {

	/* the speed the background scrolls at */
	public float xScrollSpeed;
	public float yScrollSpeed;

	/* the material with UVs to scroll */
	private Material bgMaterial;

	void Start () {
		bgMaterial = GetComponent<Renderer> ().material;
	}

	void Update () {
		bgMaterial.mainTextureOffset = new Vector2 (Time.time * xScrollSpeed, Time.time * yScrollSpeed);
	}
}
