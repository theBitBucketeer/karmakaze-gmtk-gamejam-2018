﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour {

	/* the rect transform for the health bar */
	RectTransform rectTform;

	// Use this for initialization
	void Start () {
		this.rectTform = GetComponent<RectTransform> ();
	}

	void OnEnable () {
		EventManager.updateHealthEvent += UpdateHealth;
		EventManager.startGameEvent += StartGame;
	}

	void OnDisable () {
		EventManager.updateHealthEvent -= UpdateHealth;
		EventManager.startGameEvent -= StartGame;
	}

	void StartGame () {
		float width = 150f;
		rectTform.sizeDelta = new Vector2 (width, rectTform.rect.height);
	}

	void UpdateHealth (int health) {
		float fHealth = health;
		float width = (fHealth / 100f) * 150f;
		rectTform.sizeDelta = new Vector2 (width, rectTform.rect.height);
	}
}
