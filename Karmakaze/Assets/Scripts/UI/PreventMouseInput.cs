﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PreventMouseInput : MonoBehaviour {

	GameObject lastSelectedUI;

	// Use this for initialization
	void Start () {
		lastSelectedUI = new GameObject ();
	}
	
	// Update is called once per frame
	void Update () {
		if (EventSystem.current.currentSelectedGameObject == null) {
			EventSystem.current.SetSelectedGameObject(lastSelectedUI);
		} else {
			lastSelectedUI = EventSystem.current.currentSelectedGameObject;
		}
	}
}
