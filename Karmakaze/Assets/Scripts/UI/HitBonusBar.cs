﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HitBonusBar : MonoBehaviour {

	/* the rect transform for the hit bonus bar */
	private RectTransform rectTform;

	/* the image of the bar */
	private Image bar;

	/* housekeeping for color swapping */
	private bool isIncreasing = true;

	// Use this for initialization
	void Start () {
		this.rectTform = GetComponent<RectTransform> ();
		this.bar = GetComponent<Image> ();
	}

	void OnEnable () {
		EventManager.hitBonusEvent += HitBonus;
		EventManager.startGameEvent += StartGame;
	}

	void OnDisable () {
		EventManager.hitBonusEvent -= HitBonus;
		EventManager.startGameEvent -= StartGame;
	}

	void Update () {
		if (rectTform.sizeDelta.x > 0) {
			float newRG = 0f;
			if(this.isIncreasing) {
				newRG = this.bar.color.r + 1.4f * Time.deltaTime;
				if(newRG >= 1){
					this.isIncreasing = false;
				}
			} else {
				newRG = this.bar.color.r - 1.4f * Time.deltaTime;
				if(newRG <= 0.1f) {
					this.isIncreasing = true;
				}
			}
			this.bar.color = new Color(newRG, newRG, 255f);
		}
	}

	void StartGame () {
		float width = 0f;
		rectTform.sizeDelta = new Vector2 (width, rectTform.rect.height);
	}

	void HitBonus (int hitBonus) {
		float bonusF = hitBonus;
		float width = (150/49) * bonusF;
		rectTform.sizeDelta = new Vector2 (width, rectTform.rect.height);
	}
	

}
