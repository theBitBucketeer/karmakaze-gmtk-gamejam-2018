﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LivesCounter : MonoBehaviour {

	/* the images used to indicate the number of lives */
	private Image[] images;

	// Use this for initialization
	void Start () {
		images = GetComponentsInChildren<Image> ();
	}

	void OnEnable () {
		EventManager.updateLivesEvent += UpdateLives;
	}

	void OnDisable () {
		EventManager.updateLivesEvent -= UpdateLives;
	}

	private void StartGame () {
		for(int i = 0; i < images.Length; i++) {
			images [i].enabled = true;
		}
	}

	private void UpdateLives (int lives) {
		int numImagesToRender = images.Length - lives;
		int stopIndex = images.Length - numImagesToRender;

		for(int i = 0; i < numImagesToRender; i++) {
			images [i].enabled = false;
		}
	}
}
