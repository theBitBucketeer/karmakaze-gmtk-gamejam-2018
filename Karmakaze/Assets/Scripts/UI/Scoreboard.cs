﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scoreboard : MonoBehaviour {

	/* The amount of points currently */
	private int score = 0;

	/* the text object for the score board */
	public Text scoreboard;

	// Use this for initialization
	void Start () {
		score = 0;
	}

	void OnEnable () {
		EventManager.updateScoreEvent += UpdateScore;
		EventManager.startGameEvent += StartGame;
	}

	void OnDisable () {
		EventManager.updateScoreEvent -= UpdateScore;
		EventManager.startGameEvent -= StartGame;
	}

	private void UpdateScore (int points) {
		this.score += points;
		this.scoreboard.text = this.score.ToString ();
	}

	private void StartGame () {
		this.score = 0;
		this.scoreboard.text = this.score.ToString ();
	}
}
