﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Scripts.UI {
	
	public class MainMenuController : MonoBehaviour {

		/* the main menu canvas */
		public Canvas mainMenuCanvas;

		/* the in game canvas */
		public Canvas inGameCanvas;

		/* the sound for hitting start */
		public AudioSource startSound;

		void OnEnable () {
			EventManager.gameOverEvent += GameOver;
		}

		void OnDisable () {
			EventManager.gameOverEvent -= GameOver;
		}

		public void StartGame () {
//			this.startSound.Play ();
			EventManager.StartGame ();
			mainMenuCanvas.enabled = false;
			inGameCanvas.enabled = true;
		}

		public void GameOver () {
			inGameCanvas.enabled = false;
			mainMenuCanvas.enabled = true;
		}

		public void QuitGame () {
			#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
			#else
			Application.Quit();
			#endif
		}
	}
}
